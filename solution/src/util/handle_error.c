#include "../../include/util/handle_error.h"

static char* errors[] = {
        [WRONG_ARGS] = "Wrong amount of arguments",
        [FILE_OPEN_ERR] = "Can't open file",
        [FILE_CLOSE_ERR] = "Error while closing file",
        [READ_ERR] = "Can't read file",
        [WRITE_ERR] = "Can't write in file",
        [SEEK_ERR] = "Seek error",
        [ALLOCATION_ERR] = "Allocation error"
};

void handle_err(enum status st) {
    char * err = errors[st];
    fprintf(stderr, "%s", err);
}
