#include "../include/files/file.h"
#include "../include/img/bmp.h"
#include "../include/transformation/rotate.h"
#include "../include/util/handle_error.h"

int main( int argc, char** argv ) {
    enum status st;
    if (argc != 3) {
        handle_err(WRONG_ARGS);
        return WRONG_ARGS;
    }

    struct image img = {0};
    if ((st = bmp_open(argv[1], &img)) != OK) {
        handle_err(st);
        return st;
    }
    if ((st = rotate(&img)) != OK) {
        handle_err(st);
        return st;
    }
    if ((st = bmp_close(argv[2], &img)) != OK) {
        handle_err(st);
        return st;
    }

    image_free(img);

    return OK;
}

