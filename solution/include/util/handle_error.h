//
// Created by dokerplp on 05.01.2022.
//

#ifndef LAB3_HANDLE_ERROR_H
#define LAB3_HANDLE_ERROR_H
#include "../../include/util/status.h"
#include <stdio.h>

void handle_err(enum status st);

#endif //LAB3_HANDLE_ERROR_H
